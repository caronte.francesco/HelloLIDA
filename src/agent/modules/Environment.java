/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agent.modules;

import agent.shared.DataNum1;
import agent.shared.DataNum2;
import edu.memphis.ccrg.lida.environment.EnvironmentImpl;
import edu.memphis.ccrg.lida.framework.tasks.FrameworkTaskImpl;
import java.util.Map;

/**
 *
 * @author Francesco
 */
public class Environment extends EnvironmentImpl {

    int num1 = -1;
    int num2 = -1;
    int res = 0;

    @Override
    public void init() {
        super.init(); //To change body of generated methods, choose Tools | Templates.
        num1 = (int) (Math.random() * 10);
        num2 = (int) (Math.random() * 10);
        DataNum1.setNum1(num1);
        DataNum2.setNum2(num2);
        int ticksPerRun = (Integer) getParam("ticksPerRun", 100);
        BackgroundTask backgroundTask = new BackgroundTask(ticksPerRun);
        taskSpawner.addTask(backgroundTask);
    }

    @Override
    public void resetState() {

    }

    @Override
    public void processAction(Object action) {
        if (action.toString().equals("Res")) {
            res = DataNum1.getNum1() + DataNum2.getNum2();
        }
    }

    @Override
    public Object getState(Map<String, ?> params) {
        return num1 + " " + num2;
    }

    public int getNum1() {
        return num1;
    }

    public void setNum1(int num1) {
        this.num1 = num1;
    }

    public int getNum2() {
        return num2;
    }

    public void setNum2(int num2) {
        this.num2 = num2;
    }

    public int getRes() {
        return res;
    }

    public void driveEnvironment() {
        num1 = (int) (Math.random() * 10);
        num2 = (int) (Math.random() * 10);
        DataNum1.setNum1(num1);
        DataNum2.setNum2(num2);
    }

    private class BackgroundTask extends FrameworkTaskImpl {

        public BackgroundTask(int ticksPerRun) {
            super(ticksPerRun);
        }

        @Override
        protected void runThisFrameworkTask() {
            driveEnvironment();
        }
    }

}
