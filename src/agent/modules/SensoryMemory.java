/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agent.modules;

import edu.memphis.ccrg.lida.framework.tasks.TaskManager;
import edu.memphis.ccrg.lida.sensorymemory.SensoryMemoryImpl;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Francesco
 */
public class SensoryMemory extends SensoryMemoryImpl {

    private Map<String, Object> sensorParam = new HashMap<String, Object>();
    private String val;

    private static final Logger logger = Logger.getLogger(SensoryMemory.class.getCanonicalName());

    @Override
    public void runSensors() {
        val = (String) environment.getState(sensorParam);
        logger.log(Level.INFO, "Run sensors " + val, TaskManager.getCurrentTick());
    }

    @Override
    public Object getSensoryContent(String modality, Map<String, Object> params) {
        String get = params.get("mode").toString();
        if (get.equals("num")) {
            return val;
        }
        return null;
    }

    @Override
    public void decayModule(long ticks) {

    }

}
