/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agent.modules;

import agent.shared.DataNum1;
import agent.shared.DataNum2;
import agent.shared.DataRes;
import edu.memphis.ccrg.lida.episodicmemory.EpisodicMemoryImpl;
import edu.memphis.ccrg.lida.episodicmemory.LocalAssociationListener;
import edu.memphis.ccrg.lida.framework.ModuleListener;
import edu.memphis.ccrg.lida.framework.shared.ElementFactory;
import edu.memphis.ccrg.lida.framework.shared.Node;
import edu.memphis.ccrg.lida.framework.shared.NodeStructure;
import edu.memphis.ccrg.lida.framework.tasks.TaskManager;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Francesco
 */
public class DeclarativeMemory extends EpisodicMemoryImpl {

    private static final Logger logger = Logger.getLogger(DeclarativeMemory.class.getCanonicalName());
    int x;
    int y;

    private List<LocalAssociationListener> localAssocListeners = new ArrayList<LocalAssociationListener>();

    @Override
    public void receiveCue(NodeStructure ns) {
        logger.log(Level.INFO, "ReceiveCue", TaskManager.getCurrentTick());
        DataRes.setRes(DataNum1.getNum1() + DataNum2.getNum2());
    }

    @Override
    public void addListener(ModuleListener listener) {
        if (listener instanceof LocalAssociationListener) {
            localAssocListeners.add((LocalAssociationListener) listener);
        } else {
            logger.log(Level.WARNING, "tried to add listener {0} but LocalAssociationListener is required",
                    listener);
        }
    }

    public static Node getNode(NodeStructure ns, String label) {
        for (Node n : ns.getNodes()) {
            if (n.getLabel().equals(label)) {
                return n;
            }
        }
        return null;
    }

    private void sendLocalAssociation(NodeStructure localAssociation) {
        logger.log(Level.FINER, "Sending Local Association", TaskManager
                .getCurrentTick());
        for (LocalAssociationListener l : localAssocListeners) {
            l.receiveLocalAssociation(localAssociation);
        }
    }

}
