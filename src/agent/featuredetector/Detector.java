/**
 * *****************************************************************************
 * Copyright (c) 2009, 2011 The University of Memphis.  All rights reserved.
 * This program and the accompanying materials are made available
 * under the terms of the LIDA Software Framework Non-Commercial License v1.0
 * which accompanies this distribution, and is available at
 * http://ccrg.cs.memphis.edu/assets/papers/2010/LIDA-framework-non-commercial-v1.0.pdf
 ******************************************************************************
 */
package agent.featuredetector;

import java.util.HashMap;
import java.util.Map;

import edu.memphis.ccrg.lida.pam.PamLinkable;
import edu.memphis.ccrg.lida.pam.tasks.DetectionAlgorithm;
import edu.memphis.ccrg.lida.pam.tasks.MultipleDetectionAlgorithm;

public class Detector extends MultipleDetectionAlgorithm implements DetectionAlgorithm {

    private PamLinkable num1;
    private PamLinkable num2;
    private PamLinkable res;
    private final String modality = "";
    private Map<String, Object> detectorParams = new HashMap<String, Object>();

    @Override
    public void init() {
        super.init();
        num1 = pamNodeMap.get("Num1");
        num2 = pamNodeMap.get("Num2");
        res = pamNodeMap.get("Res");
        detectorParams.put("mode", "num");
    }

    @Override
    public void detectLinkables() {
        String value = (String) sensoryMemory.getSensoryContent(modality, detectorParams);
        if (value != null) {
            String[] split = value.split(" ");

            int x = Integer.parseInt(split[0]);
            int y = Integer.parseInt(split[1]);

            if (x != -1 && y != -1) {
                pam.receiveExcitation(num1, 1.0);
                pam.receiveExcitation(num2, 1.0);
                pam.receiveExcitation(res, 1.0);
            }
        }
    }

}
