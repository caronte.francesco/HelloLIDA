/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agent.shared;

/**
 *
 * @author Francesco
 */
public final class DataNum2 {
    
    private static int num2;

    private DataNum2() {
        num2 = -1;
    }

    
    public static int getNum2() {
        return num2;
    }

    public static void setNum2(int num2) {
        DataNum2.num2 = num2;
    }
    
    
}
