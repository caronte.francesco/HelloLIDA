/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agent.shared;

/**
 *
 * @author Francesco
 */
public final class DataRes {

    private static int res;

    private DataRes() {
        res = -1;
    }

    public static int getRes() {
        return res;
    }

    public static void setRes(int res) {
        DataRes.res = res;
    }
}
