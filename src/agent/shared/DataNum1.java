/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agent.shared;

/**
 *
 * @author Francesco
 */
public final class DataNum1 {

    private static int num1;

    private DataNum1() {
        num1 = -1;
    }

    
    public static int getNum1() {
        return num1;
    }

    public static void setNum1(int num1) {
        DataNum1.num1 = num1;
    }
}
